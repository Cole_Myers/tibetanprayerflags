function reset(x, cols) {
  var container = x.parentNode;

  cols = !cols ? 1 : cols;

  if (container.offsetWidth < 552) {
    x.style.width = "100%";
    x.style.height = "570px";
  } else {
    x.style.width = "260px";
  }
  //console.log(x);

  // get container and contained objects that match x
  var container = x.parentNode;
  var elements = container.getElementsByClassName("flag-module");
  // reset opacity
  for (var i = 0; i < elements.length; i++) {
    elements[i].style.opacity = "1.0";
  }


}

function expand(x, cols) {
  var container = x.parentNode;

  cols = !cols ? 1 : cols;
  //console.log(x);
  if (container.offsetWidth < 552) {
    x.style.height = "auto";
  } else {
    x.style.width = "100%";
  }
}

function toggle(x) {
  // check if this property (expanded) has been set yet, if not set it to false
  x.expanded = !x.expanded ? false : x.expanded;

  // get container and contained objects that match x
  var container = x.parentNode;
  var elements = container.getElementsByClassName("flag-module");
  //console.log(elements);

  /*for (var i = 0; i < elements.length; i++) {
  	if (elements[i] != x) {
  		elements[i].style.opacity = "0.8";
  	}
  }*/


  switch (true) {


    // single column design
    case (container.offsetWidth < 552):
      console.log("case 1");
      console.log(container.offsetWidth);

      break;


      // two column design
    case (container.offsetWidth >= 552 && container.offsetWidth < 835):
      console.log("case 2");
      console.log(container.offsetWidth);


      var evens = container.querySelectorAll("div.flag-module:nth-child(even)");

      //console.log(odds);
      console.log(evens);

      for (var i = 0; i < evens.length; i++) {
        if (evens[i] == x) {
          // selected element is in second column
          console.log(x.previousElementSibling);
          container.insertBefore(x.previousElementSibling, x.nextSibling);
        }
      }


      break;


      // three column design
    case (container.offsetWidth >= 835):
      console.log("case 3");
      console.log(container.offsetWidth);


      var middle = container.querySelectorAll("div.flag-module:nth-child(3n - 1)");
      //console.log(odds);
      console.log(middle);

      for (var i = 0; i < middle.length; i++) {
        if (middle[i] == x) {
          // selected element is in second column
          console.log(x.previousElementSibling);
          container.insertBefore(x.previousElementSibling, x.nextSibling);
          //container.removeChild(x.previousElementSibling);
        }
      }


      var last = container.querySelectorAll("div.flag-module:nth-child(3n)");
      //console.log(odds);
      console.log(last);

      for (var i = 0; i < last.length; i++) {
        if (last[i] == x) {
          // selected element is in third column
          console.log(x.previousElementSibling);
          var second = x.previousElementSibling;
          var first = second.previousElementSibling;
          container.insertBefore(second, x.nextSibling);
          container.insertBefore(first, x.nextSibling);
          //container.removeChild(x.previousElementSibling);
        }
      }

      break;


    default:
      console.log("there was an error with the container width calculation");
      break;
  }



  if (x.expanded == false) {
    x.expanded = true;
    expand(x);
  } else {
    x.expanded = false;
    reset(x);
  }
}
